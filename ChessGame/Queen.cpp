#include "Queen.h"

Queen::Queen(const char type, const bool isEaten) : IPiece(type, isEaten), Rook(type, isEaten), Bishop(type, isEaten) { }

Queen::~Queen() { }

const char Queen::getType()
{
	return getType();
}

bool Queen::getColor()
{
	return getColor();
}

bool Queen::move(Place& src, Place& dst)
{
	return Rook::move(src, dst) || Bishop::move(src, dst);
}