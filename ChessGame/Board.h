#pragma once
#include <iostream>
#include <vector>
#include <string>
#include "Place.h"
#include "Rook.h"
#include "Pawn.h"
#include "Queen.h"
#include "King.h"
#include "Knight.h"
#include "Bishop.h"

#define BOARD_LENGTH 8
#define BOARD_WIDTH 8

class Board 
{
public:
	Board(std::string board); // constractor for board
	void operator=(const Board& other);
	~Board(); // destractor for board
	std::vector<std::vector<Place*>>& getBoard(); // returns the status of the board
	void movePieceOnBoard(Place& src, Place& dst); // moves the piece on the board
private:
	std::vector<std::vector<Place*>> _board;
};