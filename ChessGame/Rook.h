#pragma once
#include "IPiece.h"

class Rook : virtual public IPiece
{
	public:
		Rook(char type, bool isEaten);
		~Rook();
		bool move(Place& src, Place& dst) override;
};
