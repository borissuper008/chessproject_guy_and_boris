#include "Rook.h"

Rook::Rook(const char type, const bool isEaten) : IPiece(type, isEaten)
{

}

Rook::~Rook()
{
	
}

bool Rook::move(Place& src, Place& dst)
{	
	return (src.getLocation()[0] == dst.getLocation()[0] || src.getLocation()[1] == dst.getLocation()[1]);
}