#pragma once
#include "IPiece.h"
#include "Board.h"
#include "Rook.h"
#include "King.h"
#include "Knight.h"
#include <iostream>

class Rook;
class King;

class ChessGame
{
	public:
		ChessGame(std::string board); // constractor for ChessGame
		~ChessGame(); // destractor for CheeGame

		bool check(Place& piece, Place& king); // checks if the king is in check
		bool rookCheck(Place& rook, Place& king); // checks if the rook is checking the king
		bool knightCheck(Place& knight, Place& king);
		bool bishopCheck(Place& bishop, Place& king);
		bool queenCheck(Place& queen, Place& king);
		bool pawnCheck(Place& pawn, Place& king);

		bool isSameColor(Place& src, Place& dst);

		std::string putsKingInCheck(Place& src, Place& dst); // checks if the piece is puting the king in check
		Place findKing();
		std::string isValidMoveSourceDest(Place& src, Place& dst); // checks if the piece did a valid move
		std::string isSamePlace(Place& src, Place& dst); // checks if the piece is trying to move to the same place
		std::string isLegalMove(Place& src, Place& dst); // checks if the piece did a legal move

		bool checkAndReturnResult(const std::string& result);
		bool playerTurn(); // returns whos turn it is
		std::string playMove(Place& src, Place& dst); // plays the move
		void undoMove(Place& src, Place& dst); // undoes the move
		void displayBoard(); // displays the board

		IPiece* getBoard(int x, int y);
		
		Place getPlace(std::string location); // retuns the place of the piece

	private:
		bool _whiteTurn;
		Board _board;
		Place _emptyPlace;
};