#include "ChessGame.h"

ChessGame::ChessGame(std::string board) : _board(board)
{
    if (board[64] == '0')
        _whiteTurn = true;

    else
        _whiteTurn = false;
}

ChessGame::~ChessGame() { }

bool ChessGame::check(Place& piece, Place& king)
{
    char pieceType = tolower(piece.getPiece()->getType());

    switch (pieceType) 
    {
        case 'r':
            return rookCheck(piece, king);

        case 'n':
            return knightCheck(piece, king);

        case 'b':
            return bishopCheck(piece, king);

        case 'q':
            return queenCheck(piece, king);

        case 'p':
            return pawnCheck(piece, king);

        default:
            return false;  
    }
}

bool ChessGame::rookCheck(Place& rook, Place& king)
{
    if (!isSameColor(rook, king))
    {
        return false;
    }

    if (rook.getLocation()[0] == king.getLocation()[0]) // if the pieces are in the same x
    {
        if (rook.getLocation()[1] > king.getLocation()[1])
        {
            for (int i = king.getLocation()[1] + 1; i < rook.getLocation()[1]; i++)
            {
                if (!_board.getBoard()[rook.getLocation()[0]][i]->isEmpty())
                    return false;
            }
        }
        else
        {
            for (int i = rook.getLocation()[1] + 1; i < king.getLocation()[1]; i++)
            {
                if (!_board.getBoard()[rook.getLocation()[0]][i]->isEmpty())
                    return false;
            }
        }
        return true;
    }
    else if (rook.getLocation()[1] == king.getLocation()[1]) // if the pieces are in the same y
    {
        if (rook.getLocation()[0] > king.getLocation()[0])
        {
            for (int i = king.getLocation()[0] + 1; i < rook.getLocation()[0]; i++)
            {
                if (!_board.getBoard()[i][rook.getLocation()[1]]->isEmpty())
                    return false;
            }
        }
        else
        {
            for (int i = rook.getLocation()[0] + 1; i < king.getLocation()[0]; i++)
            {
                if (!_board.getBoard()[i][rook.getLocation()[1]]->isEmpty())
                    return false;
            }
        }
        return true;
    }
    else // if pieces are in a different x and y
    {
        return false;
    }
}

bool ChessGame::knightCheck(Place& knight, Place& king)
{
    if (!isSameColor(knight, king))
    {
        return false;
    }

    return knight.getPiece()->move(knight, king);
}

bool ChessGame::bishopCheck(Place& bishop, Place& king)
{
    if (!isSameColor(bishop, king))
    {
        return false;
    }

    if (!bishop.getPiece()->move(bishop, king))
    {
        return false;
    }

    // which way the bishop goes
    int rowDirection = (bishop.getLocation()[0] < king.getLocation()[0]) ? 1 : -1;
    int colDirection = (bishop.getLocation()[1] < king.getLocation()[1]) ? 1 : -1;

    //calc the next row/col location after the bishop move
    int row = bishop.getLocation()[0] + rowDirection;
    int col = bishop.getLocation()[1] + colDirection;

    while (row != king.getLocation()[0] && col != king.getLocation()[1])
    {
        if (!_board.getBoard()[row][col]->isEmpty())
        {
            return false;  // There is a piece in between
        }
        row += rowDirection;
        col += colDirection;
    }

    return true;
}

bool ChessGame::queenCheck(Place& queen, Place& king)
{
    if (!isSameColor(queen, king))
    {
        return false;
    }

    if (!queen.getPiece()->move(queen, king))
    {
        return false;
    }

    return bishopCheck(queen, king) || rookCheck(queen, king);
}

bool ChessGame::pawnCheck(Place& pawn, Place& king)
{
    if (!isSameColor(pawn, king))
    {
        return false;
    }

    return pawn.getPiece()->move(pawn, king);
}

bool ChessGame::isSameColor(Place& src, Place& dst)
{
    return (src.getPiece()->getColor() == dst.getPiece()->getColor()); // if two pieces are the same color
}

std::string ChessGame::isSamePlace(Place& src, Place& dst)
{
    if (src.getLocation() == dst.getLocation())
    {
        return "7";
    }

    return "";
}

Place ChessGame::findKing()
{
    bool isWhite = playerTurn();
    char kingType = isWhite ? 'K' : 'k';

    for (int i = 0; i < _board.getBoard().size(); ++i)
    {
        for (int j = 0; j < _board.getBoard()[i].size(); ++j)
        {
            Place* place = _board.getBoard()[i][j];
            IPiece* piece = place->getPiece();

            if (place && piece && piece->getType() == kingType && piece->getColor() == isWhite)
            {
                return place;
            }
        }
    }

    return _emptyPlace;
}

std::string ChessGame::putsKingInCheck(Place& src, Place& dst)
{
    Board movePiece = _board;
    movePiece.movePieceOnBoard(src, dst);
    Place opponentKing = findKing();

    if (check(src, opponentKing))
    {
        return "1";
    }
    if (check(opponentKing, dst))
    {
        return "4";
    }
    
    return "";
}

std::string ChessGame::isValidMoveSourceDest(Place& src, Place& dst)
{
    IPiece* destinationPiece = dst.getPiece();

    if (destinationPiece)
    {
        if (destinationPiece->getColor() == src.getPiece()->getColor())
        {
            return "3"; // Same color
        }
    }
    if (src.getPiece()->getColor() != playerTurn())
    {
        return "2";
    }
    if (!src.getPiece()->move(src, dst))
    {
        return "6"; 
    }
    return "";
}

bool ChessGame::checkAndReturnResult(const std::string& result)
{
    return result == "";
}

std::string ChessGame::isLegalMove(Place& src, Place& dst)
{
    if (!checkAndReturnResult(isSamePlace(src, dst))) 
    {
        return isSamePlace(src, dst);
    }
    else if (!checkAndReturnResult(putsKingInCheck(src, dst))) 
    {
        return putsKingInCheck(src, dst);
    }
    else if (!checkAndReturnResult(isValidMoveSourceDest(src, dst))) 
    {
        return isValidMoveSourceDest(src, dst);
    }
    return "0";
}

bool ChessGame::playerTurn()
{
    return _whiteTurn;
}

void ChessGame::displayBoard()
{   

    for (int i = 7; i > -1; i--)
    {
        for (int j = 0; j < BOARD_WIDTH; j++)
        {
            if (_board.getBoard()[i][j]->getPiece() == nullptr)
                std::cout << "#";
            else
                std::cout << _board.getBoard()[i][j]->getPiece()->getType();
            std::cout << " ";
        }
        std::cout << std::endl;
    }
}

std::string ChessGame::playMove(Place& src, Place& dst)
{
    std::string result;

    _board.movePieceOnBoard(src, dst);
    result = isLegalMove(src, dst);

    if (result != "0")
    {
        undoMove(dst, src);
    }
    else
    {
        _whiteTurn = !_whiteTurn;
    }
    return result;
}

void ChessGame::undoMove(Place& src, Place& dst)
{
    _board.movePieceOnBoard(dst, src);
}

Place ChessGame::getPlace(std::string location)
{
    std::vector<std::vector<Place*>> board = _board.getBoard();
    int x = location[1] - 49, y = location[0] - 97;
    Place place(location, board[x][y]->getPiece());

    return place;
}

IPiece* ChessGame::getBoard(int x, int y)
{
    return _board.getBoard()[x][y]->getPiece();
}
