#include "Knight.h"

Knight::Knight(const char type, const bool isEaten) : IPiece(type, isEaten) { }

Knight::~Knight() { }

const char Knight::getType()
{
	return getType();
}

bool Knight::getColor()
{
	return getColor();
}

bool Knight::move(Place& src, Place& dst)
{
	int rowDiff = std::abs(src.getLocation()[0] - dst.getLocation()[0]);
	int colDiff = std::abs(src.getLocation()[1] - dst.getLocation()[1]);

	return (rowDiff == 2 && colDiff == 1) || (rowDiff == 1 && colDiff == 2);
}