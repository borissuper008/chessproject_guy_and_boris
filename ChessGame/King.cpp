#include "King.h"

King::King(const char type, const bool isEaten) : IPiece(type, isEaten) { }

King::~King() { }

const char King::getType()
{
	return getType();
}

bool King::getColor()
{
	return getColor();
}

bool King::move(Place& src, Place& dst)
{
	int row = abs(src.getLocation()[0] - dst.getLocation()[0]);
	int col = abs(src.getLocation()[1] - dst.getLocation()[1]);
	return col <= 1 && row <= 1;
}
