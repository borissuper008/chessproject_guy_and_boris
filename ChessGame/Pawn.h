#pragma once
#include "IPiece.h"

class Pawn : public IPiece
{
	public:
		Pawn(const char type, const bool isEaten);
		~Pawn();
		const char getType();
		bool getColor();
		bool move(Place& src, Place& dst) override;
		void firstMove();
		bool hasMoved() const;
	private:
		bool _firstMove;
};
