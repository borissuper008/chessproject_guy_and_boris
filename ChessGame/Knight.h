#pragma once
#include "IPiece.h"

class Knight : public IPiece
{
	public:
		Knight(const char type, const bool isEaten);
		~Knight();
		const char getType();
		bool getColor();
		bool move(Place& src, Place& dst) override;
};
