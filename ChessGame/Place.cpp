#include "Place.h"

Place::Place()
{
	_location = " ";
	_piece = nullptr;
}

Place::Place(Place* place) 
{
	this->_location = place->getLocation();
	this->_piece = place->getPiece();
}

Place::Place(const std::string& location, IPiece* piece) : _location(location), _piece(piece) { }

void Place::operator=(const Place& other)
{
	_location = other._location;
	_piece = other._piece;
}

Place::~Place() { }

std::string Place::getLocation()
{
	return _location;
}

IPiece* Place::getPiece()
{
	return _piece;
}

void Place::setPiece(IPiece* piece)
{
	this->_piece = piece;
}

bool Place::isEmpty()
{
	return _piece == nullptr;
}
