#pragma once
#include "IPiece.h"

class Bishop : virtual public IPiece
{
	public:
		Bishop(const char type, const bool isEaten);
		~Bishop();
		const char getType();
		bool getColor();
		bool move(Place& src, Place& dst) override;
};
