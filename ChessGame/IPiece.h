#pragma once
#include <string>
#include <iostream>
#include "Place.h"

class Place;

class IPiece {
public:
	IPiece(char type, bool isEaten); // constracor for IPiece
	void operator=(const IPiece& other);
	virtual ~IPiece(); // destractor for IPiece
	virtual bool move(Place& src, Place& dst) = 0; // function will move the piece
	bool getColor() const; // returns the color of the piece
	char getType() const; // return what piece it is
	bool getIsEaten() const; // returns if the piece is eaten


private:
	bool _isEaten;
	char _type;
	bool _color;
};