#include "Pawn.h"

Pawn::Pawn(const char type, const bool isEaten) : IPiece(type, isEaten) 
{
	_firstMove = false;
}

Pawn::~Pawn() { }

const char Pawn::getType()
{
	return getType();
}

bool Pawn::getColor()
{
	return getColor();
}

bool Pawn::move(Place& src, Place& dst)
{
    int col = src.getLocation()[1] - dst.getLocation()[1];

    if (abs(col) == 1 || abs(col) == 2)
    {
        _firstMove = false;
        return true;
    }
    else if (col == 0)
    {
        if (_firstMove)
        {
            return false;
        }
        else
        {
            _firstMove = false;
            return true;
        }
    }

    return false;
}

void Pawn::firstMove()
{
    _firstMove = false;
}

bool Pawn::hasMoved() const
{
    return !_firstMove;
}
