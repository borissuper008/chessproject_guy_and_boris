#pragma once
#include "Bishop.h"
#include "Rook.h"

class Queen : public Bishop, public Rook
{
	public:
		Queen(const char type, const bool isEaten);
		~Queen();
		const char getType();
		bool getColor();
		bool move(Place& src, Place& dst) override;
};
