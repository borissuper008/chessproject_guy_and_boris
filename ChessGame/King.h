#pragma once
#include "IPiece.h"

class King : public IPiece
{
public:
	King(const char type, const bool isEaten);
	~King();
	const char getType();
	bool getColor();
	bool move(Place& src, Place& dst) override;

private:
};
