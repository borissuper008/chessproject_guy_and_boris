#include "Board.h"

Board::Board(std::string board)
{
    _board.resize(BOARD_LENGTH, std::vector<Place*>(BOARD_WIDTH));
    for (int i = 0; i < BOARD_LENGTH; i++)
    {
        for (int j = 0; j < BOARD_WIDTH; j++)
        {
            char pieceChar = board[i * BOARD_WIDTH + j];
            char column = 'a' + j;

            IPiece* piece = nullptr;

            if (pieceChar != '#')
            {
                switch (tolower(pieceChar))
                {
                case 'p':
                    piece = new Pawn(pieceChar, false);
                    break;

                case 'r':
                    piece = new Rook(pieceChar, false);
                    break;

                case 'q':
                    piece = new Queen(pieceChar, false);
                    break;

                case 'k':
                    piece = new King(pieceChar, false);
                    break;

                case 'n':
                    piece = new Knight(pieceChar, false);
                    break;

                case 'b':
                    piece = new Bishop(pieceChar, false);
                    break;
                }
            }

            _board[i][j] = new Place(column + std::to_string(i + 1), piece);
        }
    }
}

void Board::operator=(const Board& other)
{
    _board = other._board;
}

Board::~Board()
{
    for (int i = 0; i < BOARD_LENGTH; ++i) 
    {
        for (int j = 0; j < BOARD_WIDTH; ++j)
        {
            delete _board[i][j];
        }
    }
}

std::vector<std::vector<Place*>>& Board::getBoard()
{
    return this->_board;
}

void Board::movePieceOnBoard(Place& src, Place& dst)
{
    std::string srcLocation = src.getLocation(), dstLocation = dst.getLocation();
    int srcX = srcLocation[1] - 49, srcY = srcLocation[0] - 97,
            dstX = dstLocation[1] - 49, dstY = dstLocation[0] - 97;
    _board[dstX][dstY]->setPiece(src.getPiece());
    _board[srcX][srcY]->setPiece(nullptr);
}
