#pragma once
#include "IPiece.h"

class IPiece;

class Place {
public:
	Place();
	Place(Place* place);
	Place(const std::string& location, IPiece* piece); // consrtactor for Place
	void operator=(const Place& other);
	~Place(); // destractor for place
	std::string getLocation(); // returns the location of the piece on the board
	IPiece* getPiece(); // returns the piece type
	void setPiece(IPiece* piece);
	bool isEmpty(); // checks and returns if the place on the board is empty

private:
	std::string _location;
	IPiece* _piece;
};