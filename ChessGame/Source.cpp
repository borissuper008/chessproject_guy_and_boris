#include "Pipe.h"
#include "ChessGame.h"
#include <iostream>
#include <thread>

using std::cout;
using std::endl;
using std::string;


void main()
{
	system("Explorer chessGraphics.exe");

	Sleep(1000);

	srand(time_t(NULL));


	Pipe p;
	bool isConnect = p.connect();

	string ans;
	while (!isConnect)
	{
		cout << "cant connect to graphics" << endl;
		cout << "Do you try to connect again or exit? (0-try again, 1-exit)" << endl;
		std::cin >> ans;

		if (ans == "0")
		{
			cout << "trying connect again.." << endl;
			Sleep(5000);
			isConnect = p.connect();
		}
		else
		{
			p.close();
			return;
		}
	}


	char msgToGraphics[1024];
	// msgToGraphics should contain the board string accord the protocol
	// YOUR CODE
	
	strcpy_s(msgToGraphics, "rnbkqbnrpppppppp################################PPPPPPPPRNBKQBNR0"); 

	std::string board = msgToGraphics;
	//ChessGame b(board);
	ChessGame* b = new ChessGame(board);
	// IPiece* c = b->getBoard(6, 4);

	p.sendMessageToGraphics(msgToGraphics);

	// get message from graphics
	string msgFromGraphics = p.getMessageFromGraphics();

	while (msgFromGraphics != "quit")
	{
		//b->displayBoard();
		Place src = b->getPlace(msgFromGraphics.substr(0, 2));
		Place dst = b->getPlace(msgFromGraphics.substr(2, 4));
		// should handle the string the sent from graphics
		// according the protocol. Ex: e2e4           (move e2 to e4)
		char bor[1024];
		bor[0] = b->playMove(src, dst)[0];
		bor[1] = 0;	
		// YOUR CODE
		strcpy_s(msgToGraphics, bor); // msgToGraphics should contain the result of the operation
		
		// return result to graphics		
		p.sendMessageToGraphics(msgToGraphics);
		
		// get message from graphics
		msgFromGraphics = p.getMessageFromGraphics();
	}
	delete b;
	p.close();
}