#include "Bishop.h"

Bishop::Bishop(const char type, const bool isEaten) : IPiece(type, isEaten) { }

Bishop::~Bishop() { }

const char Bishop::getType()
{
	return getType();
}

bool Bishop::getColor()
{
	return getColor();
}

bool Bishop::move(Place& src, Place& dst)
{
	int rowDiff = abs(src.getLocation()[0] - dst.getLocation()[0]);
	int colDiff = abs(src.getLocation()[1] - dst.getLocation()[1]);

	return rowDiff == colDiff;
}