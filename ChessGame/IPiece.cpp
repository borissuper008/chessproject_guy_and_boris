#include "IPiece.h"

IPiece::IPiece(const char type, const bool isEaten) :  _type(type), _isEaten(isEaten) 
{
	_color = true;
}

void IPiece::operator=(const IPiece& other)
{
	_type = other._type;
	_color = other._color;
	_isEaten = other._isEaten;
}

IPiece::~IPiece() { }

bool IPiece::getColor() const
{
	return this->_color;
}

char IPiece::getType() const
{
	return this->_type;
}

bool IPiece::getIsEaten() const
{
	return this->_isEaten;
}
